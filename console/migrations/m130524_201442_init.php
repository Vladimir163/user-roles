<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    protected function getTables()
    {
        return [

            '{{%user}}' => [
                'id'                    => $this->primaryKey(),
                'id_role'               => $this->integer()->notNull(),
                'username'              => $this->string()->notNull()->unique(),
                'auth_key'              => $this->string(32)->notNull(),
                'password_hash'         => $this->string()->notNull(),
                'password_reset_token'  => $this->string()->unique(),
                'email'                 => $this->string()->notNull()->unique(),
                'status'                => $this->smallInteger()->notNull()->defaultValue(10),
                'created_at'            => $this->integer()->notNull(),
                'updated_at'            => $this->integer()->notNull(),
            ],


            'user_rights' => [
                'id_user'   => $this->integer(),
                'id_right'  => $this->integer()
            ],

            'user_ban_rights' => [
                'id_user'   => $this->integer(),
                'id_right'  => $this->integer()
            ],

        ];
    }

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql')
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        foreach ($this->getTables() as $tableName => $columns)
            $this->createTable($tableName, $columns, $tableOptions);
    }

    public function down()
    {
        foreach ($this->getTables() as $tableName => $columns)
            $this->dropTable($tableName);
    }
}
