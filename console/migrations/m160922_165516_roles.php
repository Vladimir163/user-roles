<?php

use yii\db\Migration;

class m160922_165516_roles extends Migration
{
    protected function getTables()
    {
        return [

            'roles' => [
                'id'    => $this->primaryKey(),
                'title' => $this->string(255),
            ],

            'roles_rights' => [
                'id_role'   => $this->integer(),
                'id_right'  => $this->integer()
            ],

        ];
    }
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql')
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        foreach ($this->getTables() as $tableName => $columns)
            $this->createTable($tableName, $columns, $tableOptions);
    }

    public function down()
    {
        foreach ($this->getTables() as $tableName => $columns)
            $this->dropTable($tableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}