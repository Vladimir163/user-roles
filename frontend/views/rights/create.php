<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Rights */

$this->title = 'Create Rights';
$this->params['breadcrumbs'][] = ['label' => 'Rights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rights-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
