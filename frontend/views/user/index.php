<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            [
                'attribute' => 'id_role',
                'content' => function($data) {
                    if($role = \common\models\Roles::findOne($data->id_role))
                        return $role->title;

                    return 'Undefined';
                }
            ],
            [
                'attribute' => 'rights',
                'content' => function($data) {
                    $html = '';
                    foreach($data->getRights() AS $one) {
                        $html .= $one->description . '<br>';
                    }

                    return $html;
                }
            ],
             'email:email',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
