<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\Roles;
use common\models\Rights;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList([User::STATUS_DELETED => 'Deleted', User::STATUS_ACTIVE => 'Active']) ?>
    <?= $form->field($model, 'id_role')->dropDownList(\yii\helpers\ArrayHelper::map(Roles::find()->all(), 'id', 'title'), ['prompt' => ' ---- Выберите роль']) ?>

    <b>Доступные права: </b><br>
    <div class="opened-rights" data-url="<?=\yii\helpers\Url::toRoute('user/get-opened-rights')?>">
        <?=$this->render('_open-rights', ['allRights' => Rights::find()->all(), 'selectedRights' => ArrayHelper::getColumn($model->getOpenedRights(), 'id')])?>
    </div>

    <br>

    <b>Запретить пользователю: </b><br>
    <?php foreach(Rights::find()->all() AS $oneRight) { ?>
        <input name="ban_right[<?=$oneRight->id?>]" type="checkbox" value="<?=$oneRight->id?>" <?=(in_array($oneRight->id, ArrayHelper::getColumn($model->getBanRights(), 'id_right'))) ? 'checked="checked"' : ''?>><?=$oneRight->description?><br>
    <?php } ?>

    <br>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
