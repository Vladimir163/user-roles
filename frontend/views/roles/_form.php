<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  common\models\Rights;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Roles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <b>Разрешения: </b><br>
    <?php foreach(Rights::find()->all() AS $oneRight) { ?>
        <input name="rights[<?=$oneRight->id?>]" type="checkbox" value="<?=$oneRight->id?>" <?=(in_array($oneRight->id, ArrayHelper::getColumn($model->getRights(), 'id'))) ? 'checked="checked"' : ''?>><?=$oneRight->description?><br>
    <?php } ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
