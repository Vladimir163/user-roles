$(document).ready(function() {


    $('#user-id_role').on('change', function(e) {

        var id_role = ($(this).val()) ? $(this).val() : 0;
        var url = $('.opened-rights').data('url');

        console.log(id_role);

            $.ajax({
                url: url + '&id_role=' + id_role,
                type: 'GET',
                success: function (data) {
                    $('.opened-rights').html(data);
                }
            });

    });


});