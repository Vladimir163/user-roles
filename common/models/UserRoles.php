<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_roles".
 *
 * @property integer $id_user
 * @property integer $id_role
 */
class UserRoles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_role'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'id_role' => Yii::t('app', 'Id Role'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserRolesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserRolesQuery(get_called_class());
    }
}
