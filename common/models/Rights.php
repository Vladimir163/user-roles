<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rights".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 */
class Rights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @inheritdoc
     * @return RightsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RightsQuery(get_called_class());
    }

}
