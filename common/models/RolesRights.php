<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "roles_rights".
 *
 * @property integer $id_role
 * @property integer $id_right
 */
class RolesRights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles_rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_role', 'id_right'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_role' => Yii::t('app', 'Id Role'),
            'id_right' => Yii::t('app', 'Id Right'),
        ];
    }

    /**
     * @inheritdoc
     * @return RolesRightsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RolesRightsQuery(get_called_class());
    }
}
