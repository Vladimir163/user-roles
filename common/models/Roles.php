<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "roles".
 *
 * @property integer $id
 * @property string $title
 */
class Roles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     * @return RolesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RolesQuery(get_called_class());
    }

    /**
     * @return array|Rights[]
     *
     * All rights related to this role
     */
    public function getRights()
    {
        $idsRights = ArrayHelper::getColumn(
            RolesRights::find()->where(['id_role' => $this->id])->all(),
            'id_right'
        );

        $rights = Rights::find()->where(['id' => $idsRights])->all();

        return (($rights) ? $rights : []);
    }
}
