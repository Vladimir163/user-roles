<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[UserBanRights]].
 *
 * @see UserBanRights
 */
class UserBanRightsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserBanRights[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserBanRights|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function user($id_user)
    {
        return $this->andWhere(['id_user' => $id_user]);
    }
}
