<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_ban_rights".
 *
 * @property integer $id_user
 * @property integer $id_right
 */
class UserBanRights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_ban_rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_right'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'id_right' => Yii::t('app', 'Id Right'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserBanRightsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserBanRightsQuery(get_called_class());
    }
}
