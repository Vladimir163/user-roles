<?php

namespace common\models;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Rights]].
 *
 * @see Rights
 */
class RightsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Rights[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Rights|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function onRole($id_role)
    {
        $role = Roles::findOne($id_role);
        $rights = ArrayHelper::getColumn($role->getRights(), 'id');

        return $this->andWhere(['id' => $rights]);
    }
}
